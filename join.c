#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#include<ctype.h>
#include<locale.h>

#define TAM 250
#define MAXSTR 15000

#define true 1
#define false 0

typedef unsigned char bool;

long long int contaBytesJls(char *nome);
bool header(char *tipo, int *lin, int *col, int *p, FILE *f);
void nextChar(FILE *p);
FILE *abreArq(char *nome, char *modo);
void join(char *nomeIn);
void validaNome(char *str, char *nome, char *ext);
void getcNum(char *nome, char  *n1, char *n2, char *str);
int invNum(char *nome);
int obterLinCol(char *nome, int n2, int *lin, int *col, int *p, int *n);
void preenche(FILE *part, int lin, int col, int pixel, int p);
void addPart(FILE *img, FILE *part, int lin, int col, int p);
int checaPart(FILE *p, int lin, int col, int bits);

char look;
/*------------------------------------
 *
 *
 *------------------------------------*/
int main(int argc, char *argv[])
{
  //printf("Hello world!\n");
  setlocale(LC_ALL,"");
  char str[TAM];
  printf("argc: %d....\n",argc);
  if(argc == 2)
  {
    printf("Starting....\n");
	strcpy(str,argv[1]);
  }else
  {
    printf("Digite o nome do arquivo: ");
	scanf("%s",str);
  }
  join(str);
  return 0;
}

/*************************************************
 *
 *  fun��o join
 *
 *************************************************/
	/*
	*   part   - ponteiro da parti��o atual que est� sendo juntada a imagem alvo
	*   img    - ponteiro para a imagem alvo que est� sendo juntada
	*   n1     - numero da partical
	*   n2     - valor maxino de particoes
	*   lin    - numero de linhas das parti��es
	*   col    - numero de colunas das parti��es
	*   n      - numero de linha da ultima parti��o
	*   i      - contador do la�o para jun��o de parti��o
	*   p      - quantidade de bits dos pixels
	*   nome[] - nome da primeira parti��o sem exten��o
	*   ext[]  - exten��o do arquivo
	*   cn1    - recebe o valor da parti��o
	*   cn2    - receber o valor de numero total de parti��es
	*   str[]  - nome a imagem sem a representa��o de parti��o. Ex img: ImgTestX_1_de_8  str: ImgTestX
	*   aux[]  - nome a parti��o atual que sera montada
	*/
void join(char *nomeIn)
{
	FILE *part, *img;
	char nome[TAM], ext[TAM], str[TAM], aux[TAM],tipo[4];
	char cn1[15],cn2[15];
	char vl[4]={0x00,0x00,0x00,0x00};
	int n1, n2, lin, col, x, y, k, i,l,c, n, *px, np, p=0,pp, flag=false;
	px = (int*) vl;
	validaNome(nomeIn,nome,ext);
	getcNum(nome,cn1,cn2,str);
	n1 = invNum(cn1);
	printf("N1: %d\n",n1);
	n2 = invNum(cn2);
	printf("N2: %d\n",n2);
	// abre a imagem final
	printf("str: %s\n",str);
	// obtem o tamanho de uma parti��o (qtde de linha)
	k = obterLinCol(str,n2,&lin,&col,&p,&np);
	printf("k: %d Linha: %d Col: %d p: %d np: %d\n",k,lin,col,p,np);
	// verificando se foi poss�vel verificar se existe ao menos
	// um parti��o do meio e parti��o final
	sprintf(aux,"%s.pgm",str);
	img = fopen(aux,"wb");
	if(img == NULL)
	{
		printf("N�o foi poss�vel abrir o arquivo: %s.pgm\n",str);
	}
	fprintf(img,"P5\n");
	fprintf(img,"# imagem juntada pela ferramenta join %d partes\n",n2);
	if(k == 0) // imagem inv�lida, as parti��es s�o nulas
	{
		printf("Imagem com formato inv�lido\n");
		fclose(img);
		exit(1);
	} else if(k == 1) // n�o tem a parti��o final
	{
		printf("Imagem sem a parti��o final");
		fprintf(img,"%d %d\n",col,lin*n2);

	}else // imagem aberta e obtidas os valores de linha das parti��es
	{  // do meio e tb da parti��o final
	   	printf("Ok! Imagem com a parti��o meio e final\n");
		fprintf(img,"%d %d\n",col,(lin*(n2-1))+np);
	}
	fprintf(img,"%d\n",p);
	// la�o principal pegando todas as part e juntando em img

	for(i=1; i<=n2; i++)
	{
	   sprintf(aux,"%s_%d_de_%d.pgm",str,i,n2);
	   printf(" aux: %s\n",aux);
	   // pega a particao i
	   part = fopen(aux,"rb");
	   // verifica se n�o tem erro.
	   //if(part == NULL || checaPart( part, lin, col, p)) // PARTI��O COM PROBLEMAS, SER� PREENCHIDA COM ZEROS
	   if(part != NULL)
	   	   flag = header(tipo,&l,&c,&pp,part);
		   
	   if(part != NULL && flag ) // PARTI��O COM PROBLEMAS, SER� PREENCHIDA COM ZEROS
	   {
			if ( i == n2)
			    addPart(img, part, np, col, p);
		    else
			    addPart(img,part, lin, col, p);
		    fclose(part);
	    }else
		{   // sen�o add a part em img
			////////////////////////////
		    printf("Parti��o inexistente/inv�lida!\n");
		   if ( i == n2)
		   { // possibilidade de ser a ultima parti��o e se o tamanho � menor
	           // void preenche(FILE *part, int lin, int col, int pixel, int p)
	           // se tiver preenche com zeros
			   preenche(img, np,col,0x00,p);
		   }else
		   {
			   preenche(img, lin,col,0x00,p);
		   }
		}
	}
	fclose(img);
}

int obterLinCol(char *nome, int n2, int *lin, int *col, int *p, int *n)
{
	char str[TAM], aux[TAM], flag = 0, var[3];
	FILE *part;
	int i, pixel, l, c;
	for(i=1; i<=n2; i++)
	{
	   sprintf(aux,"%s_%d_de_%d.pgm",nome,i,n2);
	   printf("aux: %s\n",aux);
	   // pega a particao i
	   part = fopen(aux,"rb");
	   // if(part != NULL ) // PARTI��O COM PROBLEMAS, SER� PREENCHIDA COM ZEROS
	   if(part != NULL ) // PARTI��O COM PROBLEMAS, SER� PREENCHIDA COM ZEROS
	   {
		   if ( i == n2) // possibilidade de ser a ultima parti��o e se o tamanho � menor
		   {
	            header(str,&l,&c, p, part);
				*n = l;
				printf("i: %d final de parti��o com l: %d  c: %d p: %d n: %d\n",i,l,c,*p,*n);
				//scanf("%s",var);
			    return 2+flag;
		   }else if(flag==0)
		   {
			   header(str,lin,col, p, part);
			   *n = *lin;
			   printf("i: %d meio de parti��o com lin: %d col: %d p: %d n: %d\n",i,*lin,*col,*p,*n);
			   //scanf("%s",var);
			   flag = 1;
		   }
	       fclose(part);
	   }
	}
	/*
	 * flag  0  -> n�o tem parti��es v�lidas
	 * flag  1  -> n�o tem parti��o final
	 * flag  2  -> s� tem parti��o final
	 * flag  3  -> tem parti��es inicio e final
	*/
	return flag;
}

void addPart(FILE *img, FILE *part, int lin, int col, int p)
{
	int l, c;
	char bac;
	printf("Adicionando a parti��o...\n");
	for( l=0; l< lin; l++)
	{
		for( c=0; c< col; c++)
		{
			if( p > 255)
            {
                bac = fgetc(part);
                fputc( bac ,img);
            }
			bac = fgetc(part);
            fputc(bac,img);
		}
	}
	printf("Done...\n\n");
}

int checaPart(FILE *p, int lin, int col, int bits)
{
	int l, c;
	for( l=0; l< lin; l++)
	{
		for( c=0; c< col; l++)
		{
			if( bits > 255)
			{
				if(feof(p))
					return 0;
				fgetc(p);
			}
			if(feof(p))
			   return 0;
			fgetc(p);
		}
	}
	return 1;
}

void preenche(FILE *part, int lin, int col, int pixel, int p)
{
	int l, c;
	char dado[4];
	int *px;
	px = (int *) dado;
	*px = pixel;
	printf("preenchendo com zeros...\n");
	for(l=0; l<lin; l++)
	{
		for(c=0; c< col; c++)
		{
			if(p > 255)
			   putc(dado[1],part);
 		    putc(dado[0],part);
		}
	}
	printf("Done...\n\n");
}

/***********************************************
 *
 *
 *
 ***********************************************/
void getcNum(char *nome, char  *n1, char *n2, char *str)
{
	int tam = strlen(nome);
	int x, k;
	// pegando n1
	k=tam-1;
	if( !isalnum(nome[k]))
	{
		printf("N�o � n�mero: %c ",nome[k]);
		exit(1);
	}
	x = 0;
	while( isalnum(nome[k]) && k >=0 )
	{
		n2[x]=nome[k];
		k--;
		x++;
	}
	n2[x]='\0';
	printf("chn2: %s\n",n2);
	// pulando:  _de_
    if(nome[k]!='_')
	{
		printf("Erro: formato inv�lido. Esperando um _");
		exit(1);
	}
	k--;
	if(nome[k]!='e')
	{
		printf("Erro: formato inv�lido. Esperando um e");
		exit(1);
	}
	k--;
	if(nome[k]!='d')
	{
		printf("Erro: formato inv�lido. Esperando um d");
		exit(1);
	}
	k--;
	if(nome[k]!='_')
	{
		printf("Erro: formato inv�lido. Esperando um _");
		exit(1);
	}
	k--;
	// pegando n2
	if(!isalnum(nome[k]))
	{
		printf("N�o � n�mero: %c ",nome[k]);
		exit(1);
	}
	x = 0;
	while( isalnum(nome[k]) && k >=0 )
	{
		n1[x]=nome[k];
		k--;
		x++;
	}
	n1[x]='\0';
	printf("chn1: %s\n",n1);
	// char *strncpy(char *dest, const char *src, size_t n)
	strncpy(str,nome,k);
	str[k]='\0';
	//printf("k: %d Str: %s\n",k,str);
}

/**************************************
 *
 *
 *
 **************************************/
int invNum(char *nome)
{
	char str[15];
	int x, k;
	printf("inv nome: %s  tam: %d\n",nome, strlen(nome));
	k=strlen(nome)-1;
	for( x=0; k >= 0; k--,x++)
		str[x]=nome[k];
	str[x]='\0';
	printf("Inv: %s\n",str);
	return atoi(str);
}

/************************************************
 *
 *
 *
 ************************************************/
void validaNome(char *str, char *nome, char *ext)
{
	int tam = strlen(str);
	int x, k, pos;
	strcpy(nome,str);

	for(k=0; k < tam ; k++)
	{
      if(str[k]=='.')
		pos = k;

	}
    nome[pos]='\0';
	printf("Nome: %s\n",nome);
	for(k=pos,x=0; k < tam ; k++, x++)
	   ext[x] = str[k];
    ext[x] ='\0';
	printf("ext: %s\n",ext);
}


/**************************
 *
 *
 **************************/
long long int contaBytesJls(char *nome)
{
	FILE *p;
	long long int cont=0;
	char dado;
	p = fopen(nome,"rb");
	if(p==NULL)
	{
		printf("N�o foi pass�vel abrir o arquivo: %s\n",nome);
		exit(1);
	}

	while(!feof(p))
	{
		dado = fgetc(p);
		if(cont < 24)
        {
		  printf("pos %4lld: 0x%0x\n",cont,(char)dado);
		}
		else if(cont == 24)
			printf("\n\n");
		cont++;
	}
    cont--;
	printf("\nQuantidade de byte do arquivo: %lld\nCabe�alho: 27\nDados compactados: %lld\n",cont,cont-27);
	return cont;
	fclose(p);
}

/*********************************************************
 *
 *
 *********************************************************/
bool header(char *tipo, int *lin, int *col, int *p, FILE *f)
{
    int tam;
	char linha[TAM];
	// pegando P2
	fgets(linha,TAM,f);
	tam =strlen(linha);
	if(linha[tam-2]=='\n'||linha[tam-2]=='\r')
	  linha[tam-2] = '\0';
	if(linha[tam-1]=='\n'||linha[tam-1]=='\r')
	  linha[tam-1] = '\0';
	printf("%s\n",linha);
	//printf("Tam: %d  str: %d\n",tam,strlen(linha));
	if(strcmp(linha,"P2")!=0 && strcmp(linha,"P5")!=0)
	{
		printf("Erro ao ler o P2: arquivo com formato inv�lido");
		return false;
	}
	strcpy(tipo,linha);
	// pegando a linha coment�rio
	fgets(linha,TAM,f);
	tam =strlen(linha);
	if(linha[tam-2]=='\n'||linha[tam-2]=='\r')
	  linha[tam-2] = '\0';
	if(linha[tam-1]=='\n'||linha[tam-1]=='\r')
	  linha[tam-1] = '\0';
	printf("%s\n",linha);
	if(linha[0]!='#')
	{
		printf("Comentario n�o encontrado na linha 2: arquivo com formato inv�lido");
		return false;
	}
	// pegnado linha e coluna
	fgets(linha,TAM,f);
	tam =strlen(linha);
	if(linha[tam-2]=='\n'||linha[tam-2]=='\r')
	  linha[tam-2] = '\0';
	if(linha[tam-1]=='\n'||linha[tam-1]=='\r')
	  linha[tam-1] = '\0';
	if(sscanf(linha,"%d %d",col, lin) !=2)
	{
		printf("arquivo com formato inv�lido");
		return false;
	}
	printf("linha: %d\nCpluna: %d\n",*lin,*col);
	// pegando o valor maxino do pixel
	fgets(linha,TAM,f);
	tam =strlen(linha);
	if(linha[tam-2]=='\n'||linha[tam-2]=='\r')
	  linha[tam-2] = '\0';
	if(linha[tam-1]=='\n'||linha[tam-1]=='\r')
	  linha[tam-1] = '\0';
	if( sscanf(linha,"%d",p) != 1)
	{
		printf("arquivo com formato inv�lido");
		return false;
	}
	printf("Valor maxino do pixel: %d\n",*p);
	return true;
}


/*********************************************************
 *
 *
 *********************************************************/
void nextChar(FILE *p)
{
  look = fgetc(p);
}

/*********************************************************
 *
 *
 *********************************************************/
FILE *abreArq(char *nome, char *modo)
{
	FILE *p;
	p = fopen(nome, modo);
	if( p == NULL)
	{
		printf("N�o foi poss�vel abrir o arquivo: %s",nome);
		exit(1);
	}
	return p;
}

