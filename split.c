#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#include<ctype.h>
#include<locale.h>

#define TAM 350
#define MAXSTR 15000

#define true 1
#define false 0

typedef int bool;

long long int contaBytesJls(char *nome);
void header(char *tipo, int *lin, int *col, int *p, FILE *f);
void nextChar(FILE *p);
FILE *abreArq(char *nome, char *modo);
void split(char *nomeIn, int flag);

char look;
/*--------------------------
 *
 *
 *--------------------------*/
int main(int argc, char *argv[])
{
  //printf("Hello world!\n");
  setlocale(LC_ALL,"");
  char str[TAM];
  int n=0;
  printf("argc: %d....\n",argc);
  if(argc == 3)
  {
    printf("Starting....\n");
	strcpy(str,argv[1]);
	n = atoi(argv[2]);
  }else
  {
    printf("Digite o nome do arquivo: ");
	scanf("%s",str);
  }
  split(str,n);
  return 0;
}

/*--------------------------
 *
 *
 *--------------------------*/
void split(char *nomeIn, int flag)
{
    FILE *In, *Out;
    char nomeOut[400],str[400],*ss,*sss;
    char tipo[5];
	int lin, col, p;
	int n, i, x, l,c,y,z,k,pos;
	In  = fopen(nomeIn, "rb");
	if( In == NULL)
	{
		printf("N�o foi poss�vel abrir o arquivo: %s",nomeIn);
		exit(1);
	}

    header(tipo, &lin, &col, &p, In);
    printf("%s\n",tipo);
    printf("%d %d\n",col, lin);
    printf("%d\n",p);
	if (flag==0)
	{
      printf("digite a qtde de linhas por parte: ");
      scanf("%d",&n);
	}else
      n = flag;
    strcpy(str,nomeIn);
	
    //ss = strstr(str,".");
	for(k=0; k<strlen(str); k++)
	{
      if(str[k]=='.')
		pos = k;
	  //ss = strstr(sss++,".");
	}
    str[pos]='\0';
    nextChar(In);
    y = lin;
    z = lin/n;
	printf("%d\n",z);
	if (lin%n != 0)
	  z++;
	printf("%d\n",z);
    for(i=0; y>0; i++)
    {
       if( y > n )
         x = n;
       else
         x = y;
       y-=n;
       sprintf(nomeOut,"%s_%d_de_%d.pgm",str,i+1,z);
       Out  = fopen(nomeOut, "wb");
       if( Out == NULL)
       {
          printf("N�o foi poss�vel abrir o arquivo: %s",nomeOut);
          exit(1);
       }
       fprintf(Out,"P5\n");
	   fprintf(Out,"# arquivo particionado ( %d de %d ) gerado pela ferramenta split\n",i+1,z);
	   fprintf(Out,"%d %d\n",col,x);
	   fprintf(Out,"%d\n",p);

	   for(l=0; l<x;l++)
       {
            for(c=0; c<col;c++)
            {
               if(p>255)
			   {
			      fputc(look , Out);
			      nextChar(In);
			      fputc(look , Out);
			   }else
			   {
			      fputc(look , Out);
			      nextChar(In);
			   }
            }
        }
        fclose(Out);
    }
   fclose(In);
}

/**************************
 *
 *
 **************************/
long long int contaBytesJls(char *nome)
{
	FILE *p;
	long long int cont=0;
	char dado;
	p = fopen(nome,"rb");
	if(p==NULL)
	{
		printf("N�o foi pass�vel abrir o arquivo: %s\n",nome);
		exit(1);
	}

	while(!feof(p))
	{
		dado = fgetc(p);
		if(cont < 24)
        {
		  printf("pos %4lld: 0x%0x\n",cont,(char)dado);
		}
		else if(cont == 24)
			printf("\n\n");
		cont++;
	}
    cont--;
	printf("\nQuantidade de byte do arquivo: %lld\nCabe�alho: 27\nDados compactados: %lld\n",cont,cont-27);
	return cont;
	fclose(p);
}

/*********************************************************
 *
 *
 *********************************************************/
void header(char *tipo, int *lin, int *col, int *p, FILE *f)
{
    int tam;
	char linha[TAM];
	// pegando P2
	fgets(linha,TAM,f);
	tam =strlen(linha);
	if(linha[tam-2]=='\n'||linha[tam-2]=='\r')
	  linha[tam-2] = '\0';
	if(linha[tam-1]=='\n'||linha[tam-1]=='\r')
	  linha[tam-1] = '\0';
	printf("\n%s\n",linha);
	//printf("Tam: %d  str: %d\n",tam,strlen(linha));
	if(strcmp(linha,"P2")!=0 && strcmp(linha,"P5")!=0)
	{
		printf("Erro ao ler o P2: arquivo com formato inv�lido");
		exit(1);
	}
	strcpy(tipo,linha);
	// pegando a linha coment�rio
	fgets(linha,TAM,f);
	tam =strlen(linha);
	if(linha[tam-2]=='\n'||linha[tam-2]=='\r')
	  linha[tam-2] = '\0';
	if(linha[tam-1]=='\n'||linha[tam-1]=='\r')
	  linha[tam-1] = '\0';
	printf("%s\n",linha);
	if(linha[0]!='#')
	{
		printf("Comentario n�o encontrado na linha 2: arquivo com formato inv�lido");
		exit(1);
	}
	// pegnado linha e coluna
	fgets(linha,TAM,f);
	tam =strlen(linha);
	if(linha[tam-2]=='\n'||linha[tam-2]=='\r')
	  linha[tam-2] = '\0';
	if(linha[tam-1]=='\n'||linha[tam-1]=='\r')
	  linha[tam-1] = '\0';
	if(sscanf(linha,"%d %d",col, lin) !=2)
	{
		printf("arquivo com formato inv�lido");
		exit(1);
	}
	printf("linha: %d\nCpluna: %d\n",*lin,*col);
	// pegando o valor maxino do pixel
	fgets(linha,TAM,f);
	tam =strlen(linha);
	if(linha[tam-2]=='\n'||linha[tam-2]=='\r')
	  linha[tam-2] = '\0';
	if(linha[tam-1]=='\n'||linha[tam-1]=='\r')
	  linha[tam-1] = '\0';
	if( sscanf(linha,"%d",p) != 1)
	{
		printf("arquivo com formato inv�lido");
		exit(1);
	}
	printf("Valor maxino do pixel: %d\n",*p);
}


/*********************************************************
 *
 *
 *********************************************************/
void nextChar(FILE *p)
{
  look = fgetc(p);
}

/*********************************************************
 *
 *
 *********************************************************/
FILE *abreArq(char *nome, char *modo)
{
	FILE *p;
	p = fopen(nome, modo);
	if( p == NULL)
	{
		printf("N�o foi poss�vel abrir o arquivo: %s",nome);
		exit(1);
	}
	return p;
}
